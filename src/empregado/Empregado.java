/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package empregado;

import java.util.Calendar;
import utilitarios.*;

/**
 *
 * @author fabio
 */
public class Empregado {

    private String primNome;
    private String ultNome;
    private Data dataContrato;
    private Tempo horaEntrada;
    private Tempo horaSaida;

    public Empregado() {
        this("", "");
    }

    public Empregado(String primNome, String ultNome) {
        this(primNome, ultNome, new Data(), new Tempo(), new Tempo());
    }

    public Empregado(String primNome, String ultNome, Data dataContrato, Tempo horaEntrada, Tempo horaSaida) {
        setPrimNome(primNome);
        setUltNome(ultNome);
        this.dataContrato = dataContrato;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }

    public Data getDataContrato() {
        return dataContrato;
    }

    public Tempo getHoraEntrada() {
        return horaEntrada;
    }

    public Tempo getHoraSaida() {
        return horaSaida;
    }

    public String getPrimNome() {
        return primNome;
    }

    public String getUltNome() {
        return ultNome;
    }

    public void setDataContrato(Data dataContrato) {
        this.dataContrato = dataContrato;
    }

    public void setHoraEntrada(Tempo horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public void setHoraSaida(Tempo horaSaida) {
        this.horaSaida = horaSaida;
    }

    public void setPrimNome(String primNome) {
        if (primNome.isEmpty()) {
            this.primNome = "sem nome";
        } else {
            this.primNome = primNome;
        }
    }

    public void setUltNome(String ultNome) {
        if (ultNome.isEmpty()) {
            this.ultNome = "sem nome";
        } else {
            this.ultNome = ultNome;
        }
    }

    @Override
    public String toString() {
        return ""
                + this.primNome + " " + this.ultNome
                + ", iniciou contrato na " + this.dataContrato
                + " e trabalhas das " + this.horaEntrada
                + " até as " + this.horaSaida + ".";
    }

    public double horasTrabalhoSemanal() {
        return this.horaSaida.diferencaEmSegundos(this.horaEntrada) * 5 / 60 / 60;
    }

    public int duracaoContrato() {
        Calendar h = Calendar.getInstance();
        Data hoje = new Data(h.get(Calendar.YEAR), h.get(Calendar.MONTH) + 1, h.get(Calendar.DAY_OF_MONTH));
        return hoje.diferenca(this.dataContrato);
    }
}
