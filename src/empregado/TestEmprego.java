/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package empregado;

import java.util.ArrayList;
import utilitarios.Data;
import utilitarios.Tempo;

/**
 *
 * @author fabio
 */
public class TestEmprego {
    public static void main(String[] args) {
        ArrayList empregados = new ArrayList();
        Data contrato1 = new Data(2000, 5, 3);
        Tempo entrada1 = new Tempo(8);
        Tempo saida1 = new Tempo(18, 35);
        Empregado e1 = new Empregado("João", "Pereira", contrato1, entrada1, saida1);
        empregados.add(e1);
        Data contrato2 = new Data(205, 7, 1);
        Tempo saida2 = new Tempo(19);
        empregados.add(new Empregado("Tiago", "Lisboa", contrato2, entrada1, saida2));
        
        for (Object empregado : empregados) {
            System.out.println(empregado);
            System.out.println("Horas de trabalho semanal: " + ((Empregado)empregado).horasTrabalhoSemanal() + ".");
            System.out.println("Duração contrado: " + ((Empregado)empregado).duracaoContrato() + " dias.");
        }
    }
}
