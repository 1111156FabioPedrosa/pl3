/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

/**
 *
 * @author fabio
 */
public class Veiculo {

    private static int numVeiculos = 0;
    private int niv;
    private String nomePropr;
    private int velocidade;
    private int direcao;

    public Veiculo(String nomePropr, int velocidade, int direcao) {
        this.niv = ++numVeiculos;
        setNomePropr(nomePropr);
        setVelocidade(velocidade);
        setDirecao(direcao);
    }

    /**
     * @return the niv
     */
    public int getNiv() {
        return niv;
    }

    /**
     * @return the nomePropr
     */
    public String getNomePropr() {
        return nomePropr;
    }

    /**
     * @param nomePropr the nomePropr to set
     */
    public void setNomePropr(String nomePropr) {
        this.nomePropr = nomePropr;
    }

    /**
     * @return the velocidade
     */
    public int getVelocidade() {
        return velocidade;
    }

    /**
     * @param velocidade the velocidade to set
     */
    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }

    /**
     * @return the direcao
     */
    public int getDirecao() {
        return direcao;
    }

    /**
     * @param direcao the direcao to set
     */
    public void setDirecao(int direcao) {
        this.direcao = direcao;
    }

    @Override
    public String toString() {
        return "O veiculo com o nº de identificação " + this.niv
                + " pertence a " + this.nomePropr
                + " viaja a " + this.velocidade + " Km/h"
                + " na direção " + this.direcao + " graus.";
    }
}
