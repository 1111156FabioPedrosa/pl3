/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

/**
 *
 * @author fabio
 */
public class TesteVeiculo {
    public static void main(String[] args) {
        Veiculo v1 = new Veiculo("João", 120, 30);
        Veiculo v2 = new Veiculo("Tiago", 140, 150);
        Veiculo[] veiculos = new Veiculo[2];
        veiculos[0] = v1;
        veiculos[1] = v2;
        for (int i = 0; i < veiculos.length; i++) {
            System.out.println(veiculos[i]);
        }
    }
}
