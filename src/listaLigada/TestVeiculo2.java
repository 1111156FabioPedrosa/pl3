/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listaLigada;

import veiculo.Veiculo;

/**
 *
 * @author fabio
 */
public class TestVeiculo2 {
    public static void main(String[] args) {
        Veiculo v1 = new Veiculo("João", 120, 30);
        Veiculo v2 = new Veiculo("Tiago", 140, 150);
        Veiculo v3 = new Veiculo("Pedro", 300, 45);
        
        ListaLigada l1 = new ListaLigada();
        System.out.println(l1.isVazia());
        l1.insereACabeca(v1);
        System.out.println(l1.isVazia());
        l1.listar();
        l1.insereACabeca(v2);
        l1.insereACabeca(v3);
        l1.listar();
    }
}
