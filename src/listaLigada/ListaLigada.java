package listaLigada;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabio
 */
public class ListaLigada {
    private int tamanho;
    private No cabeca;
    
    public ListaLigada() {
        this(null);
    }

    public ListaLigada(No cabeca) {
        this.tamanho = 0;
        this.cabeca = cabeca;
    }

    /**
     * @return the tamanho
     */
    public int getTamanho() {
        return tamanho;
    }

    /**
     * @return the cabeca
     */
    public No getCabeca() {
        return cabeca;
    }

    /**
     * @param cabeca the cabeca to set
     */
    public void setCabeca(No cabeca) {
        this.cabeca = cabeca;
    }
    
    public void insereACabeca(Object obj) {
        cabeca = new No(obj, cabeca);
        this.tamanho++;
    }
    
    public void removeACabeca(Object obj) {
        if (cabeca != null) {
            cabeca = cabeca.getProx();
            this.tamanho--;
        }
    }

    public void listar() {
        No aux = cabeca;
        for (int i = 0; i < this.tamanho; i++) {
            System.out.println(aux.getElemento());
            aux = aux.getProx();
        }
    }
    
    public boolean isVazia() {
        if (cabeca == null) {
            return true;
        }
        return false;
    }
}
